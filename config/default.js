module.exports = {
	// how many milliseconds to us equals an hour in our economy (1:1 would be
	// 3600000)
	//
	// NOTE: anything less than 200 can make some things wonky, for instance
	// the clock-in transactions might not process before the clock-out
	// transactions start, which throws things out of sync.
	time_scale: 200,

	// where to find our api
	endpoint: 'http://127.0.0.1:13007/api',

	// where our proto buffers are located (only change this if you have a good
	// reason to)
	protobuf_dir: `${__dirname}/../../basis/bundle/models/src/proto`,

	// the intial user we create to facilitate the creation of all our test
	// conditions and data
	bootstrap_user: {
		// arbitrary, but should probs be a uuid
		id: false,
		// arbitrary, but should be a real email
		email: false,
		// set this to the same value as ../config/config.yaml::tests.bootstrap_user_key
		// to generate a new keypair, use `node tools/keygen.js`
		pub: false,
		// set to the secret key paired to the bootstrap_user.pub key
		sec: false,
	},

	// should likely stay 128, unless you know something i don't
	service_id: 128,
};


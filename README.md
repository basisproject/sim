# Basis Sim

This project simulates a network of companies (and consumers) producing things using the Basis system as its substrate. It will strive to add some level of noise and randomness in order to approximate some level of instability and human "chance."

This project will likely start out in a rudimentary form as more of a full-network test framwork of Basis, and eventually become more complicated in its capabilities as time goes on. The hope is to set up and simulate large networks of producing entities using production/consumption that resemble real life, and measure the costs of the produced items.


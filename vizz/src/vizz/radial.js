const RADS = Math.PI / 180;

exports.place = function(i, {radius, centerx, centery, len}) {
	const start_angle = 30;
	const current_angle = start_angle + ((360 / len) * i);
	const radians = current_angle * RADS;
	return {
		x: centerx + radius * Math.cos(radians),
		y: centery + radius * Math.sin(radians),
	};
};

exports.nearest = function(px, py, {radius, centerx, centery}) {
	const vx = px - centerx;
	const vy = py - centery;
	const magv = Math.pow((vx * vx) + (vy * vy), 0.5);
	return {
		x: centerx + ((vx / magv) * radius),
		y: centery + ((vy / magv) * radius),
	};
};


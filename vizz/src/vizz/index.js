"use strict";

const util = require('../util');
const Radial = require('./radial');
const Events = require('../events');

module.exports = function Vizz(options) {
	options || (options = {});
	const bus = options.bus;
	if(!bus) throw new Error('Vizz() -- missing `options.bus`');

	const vizzbus = new Events();

	let width = 1024;
	let height = 400;
	let x = width / 2;
	let y = height / 2;
	let zoom = 1.0;
	let dragging = false;

	const zoomer = d3.zoom().on('zoom', () => svg.attr('transform', d3.event.transform))
	const svg = d3.select('.network')
		.append('svg')
		.attr('width', width)
		.attr('height', height)
		.call(zoomer)
		.append('g')
		.attr('class', 'container');

	const reset_zoom = () => {
		d3.select('.network').select('svg').call(zoomer.transform, d3.zoomIdentity.translate(x, y).scale(zoom));
	};
	reset_zoom();

	const svg_node = d3.select('.network').select('svg').node();
	document.addEventListener('keydown', function(e) {
		const keymap = {
			'0': () => reset_zoom(),
		};
		keymap[e.key] && keymap[e.key]();
	}, false);

	const node_dragger = d3.drag()
		.clickDistance(50)
		.on('drag', function() {
			const e = d3.event;
			e.subject.fx = e.x;
			e.subject.fy = e.y;
			vizzbus.emit('force-reset');
		})
		.on('end', function() {
			const e = d3.event;
			delete e.subject.fx;
			delete e.subject.fy;
			vizzbus.emit('force-reset');
		})
		.container(() => svg_node);
	vizzbus.on('node-add', () => {
		// NOTE: node-add is called AFTER we add the element to data, but BEFORE
		// we enter the data into d3, so we need a short delay to make sure d3
		// has our node
		setTimeout(() => {
			svg.select('.nodes').selectAll('.node')
				.attr('cursor', 'pointer')
				.call(node_dragger);
			svg.select('.nodes').selectAll('.node')
				.on('click', function(d) {
					vizzbus.emit('node-info', d);
				});
		}, 200);
	});
	vizzbus.on('node-info', (d) => {
		// toggle selection of our node, deselect all other nodes
		nodes.forEach((node) => {
			if(node.id == d.id) {
				node.selected = !node.selected;
				d.selected = node.selected;
			} else {
				delete node.selected;
			}
		});
		ticked();
		const company_id = d.type == 'company' ? d.id : consumer_company_map.to_company[d.id];
		bus.emit('economy:company-info', company_id);
	});

	svg.append('g')
		.attr('class', 'origin')
		.append('circle')
		.attr('r', 2)
		.attr('fill', '#ccc')
		.attr('cx', 0)
		.attr('cy', 0);
	svg.append('g').attr('class', 'links');
	svg.append('g').attr('class', 'pings');
	svg.append('g').attr('class', 'nodes');

	var d3_nodes = null;
	var d3_links = null;
	var d3_pings = null;
	const ticked = (_e) => {
		if(d3_links) {
			d3_links
				.select('line')
				.attr('x1', (d) => d.source.x)
				.attr('y1', (d) => d.source.y)
				.attr('x2', (d) => d.target.x)
				.attr('y2', (d) => d.target.y);
		}
		if(d3_pings) {
			const ping_pos = (d, attr) => {
				const source = d.direction == 1 ? d.link.source : d.link.target;
				const target = d.direction == 1 ? d.link.target : d.link.source;
				if(!source[attr] || !target[attr]) return 0;
				const progress = Math.min(1, (new Date().getTime() - d.start) / (d.end - d.start));
				return source[attr] + (progress * (target[attr] - source[attr]));
			};
			d3_pings
				.attr('cx', (d) => ping_pos(d, 'x'))
				.attr('cy', (d) => ping_pos(d, 'y'));
		}
		if(d3_nodes) {
			d3_nodes
				.attr('transform', (d) => `translate(${d.x}, ${d.y})`)
				.select('circle')
				.style('fill', (d) => (d.selected ? '#f44' : d._attr.c));
		}
	};
	const linkforce = d3.forceLink()
		.id((d) => d.id)
		.strength((d) => {
			const map = {
				'order': 1 / 200,
				'consumer-order': 1 / 600,
				'labor': 1 / 200,
				'member': 0,
				'_': 0,
			};
			const entry = map[d.type] || map['_'];
			return entry;
		})
		.distance((d) => {
			const dist_map = {
				'order': 80,
				'consumer-order': 50,
				'labor': 80,
				'member': 70,
				'_': 100,
			};
			const dist = dist_map[d.type] || dist_map['_'];
			return dist;
		});

	const map_radius = () => 6 * nodes.length;
	const radial_map = {
		'consumer': {
			radius: map_radius,
			strength: () => 1 / 5,
		},
	};
	const radial_force = (d, axis) => {
		const entry = radial_map[d.type];
		if(!entry) return d[axis];
		let xy = null;
		const filtered = nodes.filter((f) => f.type == d.type);
		const pos = filtered.findIndex((i) => i.id == d.id);
		xy = Radial.place(pos, {
			radius: entry.radius(d),
			centerx: 0,
			centery: 0,
			len: filtered.length,
		});
		return xy[axis];
	};
	const radialx = d3
		.forceX((d) => radial_force(d, 'x'))
		.strength((d) => radial_map[d.type] ? radial_map[d.type].strength() : 0);
	const radialy = d3
		.forceY((d) => radial_force(d, 'y'))
		.strength((d) => radial_map[d.type] ? radial_map[d.type].strength() : 0);
	const repulse = d3.forceManyBody()
		.strength((d) => d.type == 'consumer' ? -5 : -20)
		.distanceMax((d) => d.type == 'consumer' ? 80 : 200);
	const centerx = d3.forceX(() => 0)
		.strength((d) => d.type == 'consumer' ? -0.0004 : 0.001);
	const centery = d3.forceY(() => 0)
		.strength((d) => d.type == 'consumer' ? -0.0004 : 0.001);
	const force = d3.forceSimulation()
		.alphaDecay(1 - Math.pow(0.001, 1 / 600))
		.velocityDecay(0.3)
		.force('link', linkforce)
		.force('charge', repulse)
		.force('radialx', radialx)
		.force('radialy', radialy)
		.force('centerx', centerx)
		.force('centery', centery)
		.on('tick', ticked);
	
	vizzbus.on('force-reset', () => {
		force.alpha(1);
		force.restart();
	});

	const nodes = [];
	const links = [];
	const pings = [];

	const user_map = {};
	const company_map = {};
	const pubkey_map = {};
	const consumer_company_map = {
		to_company: {},
		to_consumer: {},
	};
	const timeouts = {};

	const exists = (collection, id) => collection.find((x) => x.id === id);
	const remove = (collection, id) => {
		const idx = collection.findIndex((x) => x.id === id);
		if(id < 0) return;
		collection.splice(idx, 1);
	};

	const add_node = (id, type, name) => {
		if(exists(nodes, id)) return;
		const place = (radius) => (Math.random() * radius * 2) - radius;
		const x = place(0.4 * map_radius());
		const y = place(0.4 * map_radius());
		const attr_map = {
			'consumer': {
				r: 12,
				c: '#448822',
				f: 10,
			},
			'company': {
				r: 15,
				c: '#333399',
				f: 12,
			},
			'_': {
				r: 5,
				c: '#666',
			},
		};
		const attr = attr_map[type] || attr_map['_'];
		const node = {
			id,
			type,
			name,
			x,
			y,
			_attr: attr,
		};
		nodes.push(node);
		vizzbus.emit('node-add', id, type, name);
		d3_nodes = svg.select('g.nodes')
			.selectAll('g.node')
			.data(nodes, (d) => d.id);
		const nodes_enter = d3_nodes
			.enter()
			.append('g')
			.attr('class', 'node');
		nodes_enter
			.append('circle')
			.attr('id', `entity-${id}`)
			.attr('r', attr.r)
			.style('fill', attr.c);
		nodes_enter
			.append('text')
			.text((d) => name.substr(0, 3))
			.attr('title', name)
			.style('text-anchor', 'middle')
			.attr('transform', `translate(0, ${attr.r / 4})`)
			.attr('font-size', attr.f)
			.style('fill', '#fff');
			
		d3_nodes = nodes_enter.merge(d3_nodes);
		force.stop();
		force.nodes(nodes);
		vizzbus.emit('force-reset');
	};

	const link_attr_map = {
		'member': {
			c: '#eee',
			w: 1,
			pc: '#999',
			pr: 3,
		},
		'labor': {
			c: '#bdd5d7',
			w: 1,
			pc: '#eee',
			pr: 3,
		},
		'order': {
			c: '#eee',
			w: 2,
			pc: '#f00',
			pr: 5,
		},
		'consumer-order': {
			c: '#eee',
			w: 1,
			pc: '#58c',
			pr: 3,
		},
		'_': {
			c: '#eee',
			w: 1,
			pc: '#f00',
			pr: 2,
		},
	};
	const add_link = (id, id_from, id_to, {type, name, timeout}) => {
		const attr = link_attr_map[type] || link_attr_map['_'];
		const existing_link = exists(links, id);
		if(timeout) {
			// unlink the order after a set amount of time
			const existing_timeout = timeouts[id];
			if(existing_timeout) {
				clearTimeout(existing_timeout);
				delete existing_timeout[id];
			}
			const new_timeout = setTimeout(() => remove_link(id), timeout);
			timeouts[id] = new_timeout;
		}
		if(existing_link) return;
		const link = {
			id,
			type,
			name,
			source: id_from,
			target: id_to,
		};
		links.push(link);
		vizzbus.emit('link-add', id, id_from, id_to, type, name);
		d3_links = svg
			.select('g.links')
			.selectAll('g.link')
			.data(links, (d) => d.id);
		const links_enter = d3_links
			.enter()
			.append('g')
			.attr('class', 'link');
		links_enter
			.append('line')
			.attr('id', `link-${id}`)
			.attr('stroke', attr.c)
			.attr('stroke-width', attr.w);
		d3_links = links_enter.merge(d3_links);
		force.stop();
		force.force('link').links(links);
		vizzbus.emit('force-reset');
	};

	const add_ping = (link, pingspec) => {
		const {id: ping_id, timeout, direction} = pingspec;
		if(!ping_id) return;
		if(exists(pings, ping_id)) return;
		const ping = {
			id: ping_id,
			start: new Date().getTime(),
			end: new Date().getTime() + timeout,
			direction,
			link: link,
		};
		pings.push(ping);
		vizzbus.emit('ping-add', link, ping, pingspec);
		setTimeout(() => remove_ping(ping_id), timeout);

		const attr = link_attr_map[link.type] || link_attr_type['_'];
		d3_pings = svg
			.select('g.pings')
			.selectAll('.ping')
			.data(pings, (d) => d.id);
		const pings_enter = d3_pings
			.enter()
			.append('circle')
			.attr('id', `ping-${ping_id}`)
			.attr('r', attr.pr)
			.attr('class', 'ping')
			.style('fill', pingspec.color || attr.pc);
		d3_pings = pings_enter.merge(d3_pings);
	};

	const remove_link = (id, options) => {
		options || (options = {});
		if(!exists(links, id)) return;
		remove(links, id);
		vizzbus.emit('link-remove', id, options);
		d3_links = svg
			.select('g.links')
			.selectAll('g.link')
			.data(links, (d) => d.id);
		const links_exit = d3_links.exit();
		links_exit
			.style('opacity', 1)
			.transition()
			.duration(options.duration || 100)
			.style('opacity', 0)
			.remove();
		//d3_links = links_exit.merge(d3_links);
		force.stop();
		force.force('link').links(links);
		vizzbus.emit('force-reset');
	};

	const remove_ping = (id) => {
		if(!exists(pings, id)) return;
		remove(pings, id);
		vizzbus.emit('ping-remove', id);
		d3_pings = svg
			.select('g.pings')
			.selectAll('.ping')
			.data(pings, (d) => d.id);
		const pings_exit = d3_pings.exit();
		pings_exit.remove();
		d3_pings = pings_exit.merge(d3_pings);
	};

	const action_johnnnyy = {
		'user.TxCreate': (tx) => {
			user_map[tx.id] = {id: tx.id, name: tx.name};
			const pubkey = util.bin_to_hex(tx.pubkey.data);
			pubkey_map[pubkey] = tx.id;
			add_node(tx.id, 'consumer', tx.name);
		},
		'company.TxCreatePrivate': (tx, pubkey) => {
			company_map[tx.id] = {id: tx.id, name: tx.name};
			if(tx.email.match(/consumer-company/i)) {
				const consumer_id = tx.email.replace(/@.*/, '')
					.split('-')
					.slice(3)
					.join('-');
				consumer_company_map.to_consumer[tx.id] = consumer_id;
				consumer_company_map.to_company[consumer_id] = tx.id;
				return;
			}
			add_node(tx.id, 'company', tx.name);
			/*
			const user_id = pubkey_map[pubkey];
			const link_id = [user_id, tx.id].join('::');
			add_link(link_id, user_id, tx.id, {
				type: 'member',
				name: user_map[user_id].name+' -> '+tx.name,
			});
			*/
		},
		/*
		'company_member.TxCreate': (tx) => {
			const user_id = tx.user_id;
			const company_id = tx.company_id;
			const link_id = [user_id, company_id, 'member'].join('::');
			add_link(link_id, user_id, company_id, {
				type: 'member',
				name: user_map[user_id].name+' -> '+company_map[company_id].name,
			});
		},
		*/
		'labor.TxCreate': (tx) => {
			const {user_id, company_id} = tx;
			const link_id = tx.id;
			add_link(link_id, user_id, company_id, {
				type: 'labor',
				name: 'labor: '+user_map[user_id].name+' -> '+company_map[company_id].name,
			});
		},
		'labor.TxUpdate': (tx) => {
			const link_id = tx.id;
			remove_link(link_id);
		},
		'order.TxCreate': (tx) => {
			let c_from = tx.company_id_from;
			let c_to = tx.company_id_to;
			const name_from = company_map[c_from].name;
			const name_to = company_map[c_to].name;
			let type = 'order';
			if(consumer_company_map.to_consumer[c_from]) {
				c_from = consumer_company_map.to_consumer[c_from];
				type = 'consumer-order';
			}
			const link_id = [tx.id, 'order'].join('::');
			add_link(link_id, c_from, c_to, {
				type,
				name: name_from+' -> '+name_to,
			});
			const link = exists(links, link_id);
			if(link) {
				add_ping(link, {
					id: ['ping', link_id].join('::'),
					timeout: 500,
					direction: 1,
				});
			}
		},
		'order.TxUpdateStatus': (tx) => {
			// ugh. this is duplicated code and needs to be kept up to date with
			// proto/order.proto::ProcessStatus
			const process_status_map = {
				0: 'unknown',
				1: 'new',
				2: 'accepted',
				3: 'processing',
				4: 'completed',
				5: 'proxying',
				6: 'finalized',
				7: 'canceled',
			};
			const status = process_status_map[tx.process_status];
			const link_id = [tx.id, 'order'].join('::');
			switch(status) {
				case 'finalized':
					const ping_id = ['ping', link_id].join('::');
					const link = exists(links, link_id);
					const duration = 500;
					if(link) {
						const newping = {
							id: ['ping', link_id, 'complete'].join('::'),
							timeout: duration,
							direction: -1,
							color: '#08c',
						};
						add_ping(link, newping);
					}
					remove_link(link_id, {duration: duration});
					break;
			}
		},
	};

	bus.on('tx', ({type, body, pubkey, debug: _debug}) => {
		//console.log('tx: ', type, body);	// DEBUG: remove
		const fn = action_johnnnyy[type];
		if(!fn) return;
		fn(body, pubkey);
	});
};


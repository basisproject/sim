const proto = require('../build/protobuf');
const Events = require('./events');
const Vizz = require('./vizz/index');
const Transactions = require('./transactions');
const EconomyInfo = require('./economy-info');

document.addEventListener('DOMContentLoaded', async () => {
	await proto.load();
	const bus = new Events();
	bus.on('error', ({type, err}) => {
		console.error('---', type, err.stack);
	});
	const trans_parser = new Transactions({bus: bus});
	const vizz = new Vizz({bus: bus});
	const info = new EconomyInfo({
		bus: bus,
		products_node: document.body.querySelector('.info-pane .products'),
		info_node: document.body.querySelector('.info-pane .company-info'),
	});
	bus.source();
});


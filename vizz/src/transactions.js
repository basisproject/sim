"use strict";

const proto = require('../build/protobuf');
const util = require('./util');

const message_id_map = (function() {
	// these MUST be ordered in the same way as basis/src/block/transactions/mod.rs
	const transactions = [
		'user.TxCreate',
		'user.TxUpdate',
		'user.TxSetPubkey',
		'user.TxSetRoles',
		'user.TxDelete',

		'company.TxCreatePrivate',
		'company.TxUpdate',
		'company.TxSetType',
		'company.TxDelete',

		'company_member.TxCreate',
		'company_member.TxSetRoles',
		'company_member.TxDelete',

		'labor.TxCreate',
		'labor.TxUpdate',

		'product.TxCreate',
		'product.TxUpdate',
		'product.TxDelete',

		'resource_tag.TxCreate',
		'resource_tag.TxDelete',

		'order.TxCreate',
		'order.TxUpdateStatus',
		'order.TxUpdateCostCategory',
	];
	const map = {};
	let i = 0;
	transactions.forEach((t) => { map[i++] = t; });
	return map;
})();

module.exports = function Transactions(options) {
	options || (options = {});
	const bus = options.bus;
	if(!bus) throw new Error('Transactions() -- missing `options.bus`');

	const type_map = {};

	const root = proto.root();
	const tx_parser = (tx) => {
		if(tx.status.type != 'success') return;
		try {
			const msg_bin = util.hex_to_bin(tx.content.message);
			const pubkey = util.bin_to_hex(msg_bin.slice(0, 32));
			const msg_exo_class = msg_bin.slice(32, 33)[0];
			const msg_exo_type = msg_bin.slice(33, 34)[0];
			// we only care about transactions
			if(msg_exo_class !== 0 || msg_exo_type !== 0) return;
			const msg_payload = msg_bin.slice(34, -64);
			const msg_service = new Uint16Array(msg_payload.slice(0, 2).buffer)[0];
			const msg_type = new Uint16Array(msg_payload.slice(2, 4).buffer)[0];
			const msg_body = msg_payload.slice(4);
			const mapped_type = message_id_map[msg_type];
			const type = root.lookupType(`basis.${mapped_type}`);
			const msg = type.decode(msg_body);
			bus.emit('tx', {type: mapped_type, body: msg, pubkey, debug: tx.content.debug});
		} catch(e) {
			bus.emit('error', util.error('tx-parse', e));
		}
	};
	bus.on('tx:raw', tx_parser);
};


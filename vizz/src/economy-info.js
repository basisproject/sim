module.exports = function EconomyInfo(options) {
	options || (options = {});
	const bus = options.bus;
	const $products = options.products_node;
	const $info = options.info_node;
	if(!bus) throw new Error('EconomyInfo() -- missing `options.bus`');

	let current_company_id = null;
	let refresh_interval = null;

	bus.on('economy:company-info', async (company_id) => {
		clear_node_info();
		if(current_company_id == company_id) {
			current_company_id = null;
			return;
		}

		current_company_id = company_id;
		refresh_node();
		refresh_interval = setInterval(refresh_node, 5000);
	});

	const clear_product_list = () => {
		$products.innerHTML = '-';
	};

	const update_product_list = (products) => {
		const elements = products.map((p) => {
			const {product, costs} = p;
			return `
				<li>
					<h3>${product.name} - ${product.id}</h3>
					<table>
						<tbody>
							<tr>
								<td><code>Labor: total</code></td>
								<td>${Object.keys(costs.labor).reduce((acc, x) => acc + costs.labor[x], 0)}</td>
							</tr>
							${Object.keys(costs.labor).sort().map((c) => {
								return `
									<tr>
										<td><code>Labor: ${c}</code></td>
										<td>${costs.labor[c]}</td>
									</tr>
								`;
							}).join('')}
							${Object.keys(costs.products).sort().map((c) => {
								return `
									<tr>
										<td><code>${c}</code></td>
										<td>${costs.products[c]}</td>
									</tr>
								`;
							}).join('')}
						</tbody>
					</table>
				</li>
			`;
		});
		$products.innerHTML = `<ul>${elements.join('')}</ul>`;
	};

	const clear_node_info = () => {
		if(refresh_interval) {
			clearInterval(refresh_interval);
			refresh_interval = null;
		}
		clear_product_list();
	};

	const refresh_node = async () => {
		try {
			var [res] = await Sexhr({
				url: `${config.endpoint}/services/basis/v1/products/by-company`,
				querydata: {
					company_id: current_company_id,
				},
			});
			const products = JSON.parse(res).items;
			update_product_list(products);
		} catch(err) {
			console.error('Error grabbing company products: ', err);
		}
	};
};


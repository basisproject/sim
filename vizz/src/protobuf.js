"use strict";

const protobuf = require('protobufjs');

const root = new protobuf.Root();

exports.root = () => root;

exports.load = async function() {
	await root.load(_protofiles, {keepCase: true});
};

// const _protofiles = [] ... filled in by makefile


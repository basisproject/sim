"use strict";

const Promise = require('bluebird');
const util = require('./util');

module.exports = function Events() {
	let _events = {};

	const grab_block = async (height) => {
		var [res] = await Sexhr({
			url: `${config.endpoint}/explorer/v1/block?height=${height}`,
		});
		return JSON.parse(res).txs;
	};

	const grab_tx = async (txid) => {
		var [res] = await Sexhr({
			url: `${config.endpoint}/explorer/v1/transactions?hash=${txid}`,
		});
		const transaction = JSON.parse(res);
		return transaction;
	};

	let initial_load_complete = false;
	const initial_load = async (seen) => {
		// load blocks and all the transactions therein
		let block_latest = null;
		let block_earliest = 0;
		while(true) {
			try {
				let url = `${config.endpoint}/explorer/v1/blocks?count=1000&skip_empty_blocks=true`;
				let qs = {
					//'latest': block_latest,
					'earliest': block_earliest,
				};
				url += '&'+Object.keys(qs)
					.filter((k) => qs[k])
					.map((k) => k+'='+encodeURIComponent(qs[k]))
					.join('&');
				var [res] = await Sexhr({
					url: url,
				});
				const parsed = JSON.parse(res);
				const blocks = parsed.blocks;
				if(blocks.length == 0) {
					break;
				}
				block_latest = '';
				block_earliest = blocks.reduce((acc, x) => Math.max(acc, x.height), 0) + 1;
				const transactions_array = await Promise.mapSeries(blocks.reverse().map((x) => x.height), grab_block);
				const transaction_ids = [];
				transactions_array.forEach((transarray) => {
					transarray.forEach((trans) => transaction_ids.push(trans.tx_hash));
				});

				await Promise.mapSeries(transaction_ids, async (txid) => {
					const transaction = await grab_tx(txid);
					seen[txid] = true;
					emit('tx:raw', transaction);
				});
			} catch(e) {
				console.warn('Events.source() -- grab: ', e);
				emit('error', util.error('events.source', e));
				await util.sleep(3000);
			}
		}
		initial_load_complete = true;
	};

	const subscribe = async (seen) => {
		const ws_url = config.endpoint.replace(/^https?:/i, 'ws:');
		try {
			const sock = new WebSocket(`${ws_url}/explorer/v1/transactions/subscribe`);
			let pre_initial_load = [];
			sock.onmessage = async (event) => {
				try {
					const transaction_meta = JSON.parse(event.data);
					const txid = transaction_meta.tx_hash;
					if(!initial_load_complete) {
						pre_initial_load.push(txid);
						return;
					}
					if(pre_initial_load) {
						pre_initial_load.push(txid);
						const unseen = pre_initial_load
							.filter((txid) => !seen[txid]);
						pre_initial_load = null;
						const transactions = await Promise.mapSeries(unseen, grab_tx);
						transactions.forEach((transaction) => {
							emit('tx:raw', transaction);
						});
						return;
					}
					const transaction = await grab_tx(txid);
					emit('tx:raw', transaction);
				} catch(e) {
					console.warn('Events.source() -- subscribe: ', e);
					emit('error', util.error('events.source', e));
				}
			};
		} catch(e) {
			console.warn('Events.source() -- subscribe: ', e);
			emit('error', util.error('events.source', e));
			await util.sleep(3000);
			subscribe(seen);
		}
	};

	const source = async () => {
		let last_event_id = -1;
		let last_instance_id = null;

		const seen = {};

		// first, subscribe to transactions, and save them until we've caught up
		// our block history parsing...then we event out the transactions that
		// we haven't seen yet.
		subscribe(seen);

		// next, load all past transactions
		await initial_load(seen);
	};

	const on = (evname, fn) => {
		const events = _events;
		if(!events[evname]) events[evname] = [];
		events[evname].push(fn);
	};

	const once = (evname, fn) => {
		var _called = false;
		on(evname, (...args) => {
			off(evname, fn);
			if(_called) return;
			_called = true;
			fn.apply(this, args);
		});
	};

	const off = (evname, fn) => {
		const events = _events;
		if(!events[evname]) return true;
		const length = events[evname].length;
		if(fn instanceof Function) {
			events[evname] = events[evname].filter((evfn) => evfn != fn);
			return events[evname].length != length;
		} else {
			events[evname] = [];
			return length > 0;
		}
	};

	const emit = (evname, ...args) => {
		const events = _events;
		if(evname != '*') {
			emit('*', evname, args);
		}
		if(!events[evname]) return;
		const scope = {};
		events[evname].forEach((evfn) => {
			evfn.apply(scope, args);
		});
	};

	const feed = async (url) => {
		var [res, _xhr] = await Sexhr({url});
		const events = JSON.parse(res);
		for(var i = 0, n = events.length; i < n; i++) {
			const ev = events[i];
			const next = events[i + 1];
			emit.apply(this, [ev.ev].concat(ev.args));
			if(next && ev.time && next.time) {
				await util.sleep(next.time - ev.time);
			}
		}
	};

	return {
		source,
		on,
		once,
		off,
		emit,
		feed,
	};
};


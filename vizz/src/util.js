exports.sleep = (ms, val) => new Promise((resolve) => setTimeout(resolve.bind(this, val), ms));

exports.error = (type, err) => ({type, err});

exports.hex_to_bin = (hexstr) => {
	return new Uint8Array(hexstr.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
};

exports.bin_to_hex = (uarr) => {
	return Array.prototype.map.call(uarr, x => ('00' + x.toString(16)).slice(-2)).join('');
};

exports.timer = (ms, cb) => {
	let triggered = false;
	let timeout = null;

	const trigger = () => {
		triggered = true;
		console.log('triggered!');
		if(cb) cb();
	};

	const stop = () => {
		if(timeout) clearTimeout(timeout);
		timeout = null;
	};

	const reset = () => {
		stop();
		triggered = false;
		timeout = setTimeout(trigger, ms);
	};

	const is_triggered = () => triggered;

	return {
		reset,
		stop,
		is_triggered,
	};
};


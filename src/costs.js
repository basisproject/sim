// how many milliseconds per tick (ie, per-hour)
const TIME_SCALE = 10;
const HOURS_IN_DAY = 8;

/*
const companies = {
	'oil': {
		cycle: HOURS_IN_DAY,
		products: {
			'super_gas': {
				inputs: [
					{raw: true, product: 'natural_gas', amount: 1.0},
				],
			},
			'super_fuel': {
				inputs: [
					{raw: true, product: 'oil', amount: 2.0},
				],
			},
		},
	},
	'coal-2-u': {
		cycle: HOURS_IN_DAY,
		products: {
			'coal': {
				inputs: [
					{raw: true, product: 'coal': amount: 1.0},
				],
			}
		},
	},
	'powerman5000': {
		cycle: HOURS_IN_DAY,
		products: {
			'electricity: {
				inputs: [
					{company: 'coal-2-u', product: 'coal': amount: 100.0},
				],
			},
		},
	},
	'fertilizer': {
		cycle: HOURS_IN_DAY,
		products: {
			'fertilizer': {
				inputs: [
					{company: 'oil', product: 'super_gas', amount: 1.0},
				],
			},
		},
	},
	'farm': {
		cycle: HOURS_IN_DAY * 5 * 52,
		products: {
			'apple': {
				inputs: [
					{company: 'fertilizer', product: 'fertilizer', amount: 1.0},
				],
			},
			'banana': {
				inputs: [
					{company: 'fertilizer', product: 'fertilizer', amount: 1.0},
				],
			},
		},
		expenses: {
			'labor': {rate: HOURS_IN_DAY * 10},
			'fuel': {company: 'oil', product: 'super_gas', amount: 1000.0},
			'electricity': {company: 'powerman5000', product: 'electricity', amount: 1000.0},
			// TODO: equipment/machines and depreciation
		},
	},
	'shipper': {
		cycle: HOURS_IN_DAY,
		expenses: {
			'labor': {rate: HOURS_IN_DAY * 10},
			'fuel': {company: 'oil', product: 'super_gas', amount: 1000.0},
			'electricity': {company: 'powerman5000', product: 'electricity', amount: 100.0},
			// TODO: equipment/machines and depreciation
			// TODO: storefront costs (avg maintenance)
		},
	},
	'grocer': {
		cycle: HOURS_IN_DAY,
		products: {
			'apple': {
				inputs: [
					{company: 'fertilizer', product: 'fertilizer', amount: 1.0},
				],
			},
			'banana': {
				inputs: [
					{company: 'fertilizer', product: 'fertilizer', amount: 1.0},
				],
			},
		},
		expenses: {
			'labor': {rate: HOURS_IN_DAY * 5},
			'electricity': {company: 'powerman5000', product: 'electricity', amount: 200.0},
			// TODO: storefront costs (avg maintenance)
		},
	},
};
*/

const company_farm = {
		cycle: HOURS_IN_DAY * 5 * 52,
		products: {
			'apple': {
				inputs: [],
			},
			'banana': {
				inputs: [],
			},
		},
		expenses: {
			'labor': {rate: HOURS_IN_DAY * 10},
			//'fuel': {company: 'oil', product: 'super_gas', amount: 1000.0},
			//'electricity': {company: 'powerman5000', product: 'electricity', amount: 1000.0},
			// TODO: equipment/machines and depreciation
		},
};

const company_grocer = {
	cycle: HOURS_IN_DAY,
	products: {
		'apple': {
			inputs: [
				{company: 'farm', product: 'apple', amount: 1.0},
			],
			inventory: 0,
		},
		'banana': {
			inputs: [
				{company: 'farm', product: 'banana', amount: 1.0},
			],
			inventory: 0,
		},
	},
	expenses: {
		'labor': {rate: HOURS_IN_DAY * 5},
		//'electricity': {company: 'powerman5000', product: 'electricity', amount: 200.0},
		// TODO: storefront costs (avg maintenance)
	},
};

function delay(ms) {
	return new Promise(function(resolve) {
		setTimeout(resolve, ms);
	});
}

function random_val(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}

async function delay_h(hours) {
	await delay(hours * TIME_SCALE);
}

function buy(product) {
}

async function run_farm() {

}

async function run_grocer() {
}

async function consumer(id) {
	function log_c(str) {
		console.log.apply(console, [`- consumer ${id}: ${str}`].concat(Array.prototype.slice.call(arguments, 1)));
	}
	const products = Object.keys(company_grocer.products);
	while(true) {
		const hours = Math.random() * 24;
		await delay_h(hours);
		const product = random_val(products);
		buy(product);
	}
}

async function main() {
	run_farm();
	run_grocer();

	const num_consumers = 8;
	for(var i = 0; i < num_consumers; i++) {
		consumer(i + 1);
	}
	while(true) {
		await delay_h(24);
	}
}

main()
	.catch(function(err) { console.log(err.stack); })
	.finally(function() { setTimeout(process.exit, 300); });


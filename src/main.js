const config = require('./helpers/config');
const Basis = require('lib-basis-client');
Basis.init(config);
const Emitter = require('events');
const Promise = require('bluebird');
const moment = require('moment');
const trans = Basis.transactions;
const Companies = require('./companies');
const Consumers = require('./consumers');
const http = require('http');
const uuid = require('uuid/v4');

const TIME_SCALE = config.time_scale;

const instance_id = uuid();

trans.clear_users();
trans.add_user('root', config.bootstrap_user.pub, config.bootstrap_user.sec);

const bus = new Emitter();
bus.setMaxListeners(9999);
const start = new Date(moment().startOf('hour').toISOString());

async function run() {
	const companies_thread = Companies.run(bus, start);
	const consumers_thread = Consumers.run(bus, start);
	let ticks = 0;
	let interval = setInterval(() => {
		ticks++;
		const time = start.getTime() + (ticks * 3600 * 1000);
		bus.emit('tick', new Date(time));
	}, TIME_SCALE);
	await Promise.all([companies_thread, consumers_thread]);
}

async function main() {
	Companies.set_bus(bus);

	// a household uses ~910 kWh/month, so we use that as a base measurement for
	// our companies (how many households of energy they use). this is obviously
	// going to be rough and inaccurate. sue me.
	//
	// note that the power company bills weekly, so we divide these numbers out
	// accordingly.
	const base_watthours = 910000 / 4;

	await Companies.create('coal-miney', {
		workers: [
			['dana', 'mine president'],
			['jerry', 'miner'],
			['larry', 'miner'],
			//['berry', 'miner'],
			//['carrie', 'miner'],
			//['terry', 'miner'],
		],
	});
	await Companies.create_product('coal-miney', 'coal', {
		unit: 'millimeter',
		mass_mg: 1.0,
		resource: true,
	});

	await Companies.create('power-planty', {
		workers: [
			['vincent', 'plant president'],
			['tessa', 'technician'],
			['valerie', 'technician'],
			['jineane', 'technician'],
			['martha', 'technician'],
		],
	});
	await Companies.create_product('power-planty', 'electricity', {
		unit: 'watthour',
		mass_mg: 0,
		inputs: [
			// `amount` based on conversion from 2,460 kWh/ton (coal) -> Wh / mg
			// https://science.howstuffworks.com/environmental/energy/question481.htm
			{company: 'coal-miney', product: 'coal', amount: ((2460 * 1000) / 907184700)},
		],
	});

	await Companies.create('farmy', {
		workers: [
			['andrew', 'farmer'],
			['thomas', 'farmer'],
			['walter', 'farmer'],
			['pat', 'farmer'],
			['linda', 'farmer'],
			['michael', 'farmer'],
			['evadne', 'farm president'],
			['alice', 'farmer'],
		],
	});
	await Companies.create_product('farmy', 'apple', {
		unit: 'each',
		mass_mg: 151197,
		effort: {time: 'hours', quantity: 1},
	});
	await Companies.create_product('farmy', 'banana', {
		unit: 'each',
		mass_mg: 120000,
		effort: {time: 'hours', quantity: 1},
	});
	await Companies.setup_order('farmy', 'power-planty', 'electricity', {
		amount: base_watthours * 4,
		period: '0 0 10 * * 2',
	});

	await Companies.create('grocer', {
		workers: [
			['dominic', 'grocer president'],
			['sasha', 'grocer'],
			['laura', 'grocer'],
			['alfred', 'grocer'],
		],
	});
	await Companies.create_product('grocer', 'apple', {
		unit: 'each',
		inputs: [{company: 'farmy', product: 'apple', amount: 1}],
		effort: {time: 'hours', quantity: 1},
	});
	await Companies.create_product('grocer', 'banana', {
		unit: 'each',
		inputs: [{company: 'farmy', product: 'banana', amount: 1}],
		effort: {time: 'hours', quantity: 1},
	});
	await Companies.setup_order('grocer', 'power-planty', 'electricity', {
		amount: base_watthours * 2,
		period: '0 0 10 * * 3',
	});

	// coal mines need power too...
	await Companies.setup_order('coal-miney', 'power-planty', 'electricity', {
		amount: base_watthours * 1,
		period: '0 0 10 * * 4',
	});
	/*
	// wow, catered lunches at the power plant!!
	await Companies.setup_order('power-planty', 'grocer', 'apple', {
		amount: Object.keys(Companies.get('power-planty').workers).length,
		period: '0 0 9 * * *',
	});
	*/

	await run();
}

main()
	.catch((err) => console.log('err: ', err.stack))
	.finally(() => setTimeout(process.exit, 300));


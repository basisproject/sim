const Basis = require('lib-basis-client');
Basis.init(__dirname+'/../config');

const Emitter = require('events');
const Promise = require('bluebird');
const companies = require('./companies');
const uuid = require('uuid/v4');
const trans = Basis.transactions;
const tx = trans.types;
const cron = require('./helpers/cron');
const util = require('./helpers/util');

const logerr = util.logerr;

const consumers = {};

exports.create = async function(uid, name, options) {
	options || (options = {});
	const company_id = uuid();
	const company_name = `(c)${name}`;
	const founder_uuid = uuid();
	const ctag_op_id = uuid();
	const ctag_inv_id = uuid();
	var res = await trans.send_as(uid, tx.company.TxCreatePrivate, {
		id: company_id,
		email: `consumer-company-${name}-${uid}@${name}.org`,
		name: company_name,
		cost_tags: [
			{id: ctag_op_id, name: 'operating'},
			{id: ctag_inv_id, name: 'inventory'},
		],
		founder: {
			member_id: founder_uuid,
			occupation: 'PRESIDENT (WITH BIG HANDS. THE BIGGEST HANDS.)',
		},
		created: new Date().toISOString(),
	});
	if(!res.success) {
		throw new Error('consumer.create() -- problem adding company: '+res.description);
	}
	// TODO: create a company for each consumer so they can order things
	const workers = {};
	workers[uid] = name;
	const consumer = {
		company: {
			id: company_id,
			name: company_name,
			bus: new Emitter(),
			workers: workers,
			cost_tags: {
				'operating': ctag_op_id,
				'inventory': ctag_inv_id,
			},
		},
		transactions: [],
		energy: 24 * 3,
	};
	consumer.company.bus.setMaxListeners(9999)
	consumers[name] = consumer;
	companies.index(consumers[name].company);
};

async function consumer_tick(now, consumer) {
	consumer.energy -= 1;
	// 6pm is time to get grocery
	if(cron.matches(now, '0 0 18 * * *') && consumer.energy < 8) {
		// wow such work, very hungry
		const grocer = companies.get('grocer');
		const eat = Math.random() < 0.5 ? 'apple' : 'banana';
		const amount = 8;
		const company_from_id = consumer.company_id;
		// yes hai 8 (apples|bananas) plz
		var res = await companies.order_product(consumer.company.id, grocer.products[eat].id, amount, 'inventory', {
			delivered: function() {
				// nomnomnom mm dis is gud nom
				consumer.energy += 24 * (2 + Math.random() * 2);	// very full...
			},
		});
		if(!res.success) {
			throw new Error('consumer.tickr() -- problem getting food: '+res.description);
		}
	}
}

function consumer_runner(bus, name) {
	console.log('> run: consumer: ', name);
	const consumer = consumers[name];
	bus.on('tick', function(now) {
		return consumer_tick(now, consumer)
			.catch((err) => {
				logerr('consumer::tick', consumer.name, err.stack);
			});
	});
}

exports.run = function(bus) {
	return Promise.map(Object.keys(consumers), function(name) {
		return consumer_runner(bus, name);
	});
};


const Basis = require('lib-basis-client');
Basis.init(__dirname+'/../config');

const Emitter = require('events');
const Promise = require('bluebird');
const moment = require('moment');
const uuid = require('uuid/v4');
const Exonum = require('exonum-client');
const cron_parser = require('cron-parser');
const config = Basis.config
const trans = Basis.transactions;
const tx = trans.types;
const proto = Basis.protobuf;
const Consumers = require('./consumers');
const cron = require('./helpers/cron');
const util = require('./helpers/util');

const logerr = util.logerr;

const TIME_SCALE = config.time_scale;

const companies = {};
const companies_idx_id = {};
const products_idx_id = {};

var start = null;
var last_time = null;
var root_bus = null;
var event_id = 0;

exports.set_bus = function(bus) {
	root_bus = bus;
};

exports.get = function(name) {
	return companies[name];
};

exports.index = function(company) {
	companies_idx_id[company.id] = company;
};

exports.create = async function(name, options) {
	options || (options = {});
	const id = uuid();
	console.log(`+ company: ${name} -- ${id}`);

	const workers = {};
	await Promise.map(options.workers || [], async function([worker_name, occupation]) {
		const uid = uuid();
		const {publicKey: pubkey, secretKey: seckey} = Exonum.keyPair();
		trans.add_user(uid, pubkey, seckey);
		var res = await trans.send_as('root', tx.user.TxCreate, {
			id: uid,
			pubkey,
			roles: ['User', 'TimeTraveler', 'TagAdmin'],
			email: `${worker_name}-${uid}@${name}.org`,
			name: worker_name,
			meta: '{}',
			created: new Date().toISOString(),
		});
		workers[uid] = {
			name: worker_name,
			member_id: uuid(),
			occupation: occupation,
		};
		if(!res.success) {
			throw new Error('company.create() -- problem adding worker: '+res.description);
		}
		await Consumers.create(uid, worker_name);
		console.log(`  + user: ${worker_name} -- ${uid}`);
	});

	const worker_ids = Object.keys(workers);
	const founder = worker_ids[0];
	const founder_uuid = uuid();
	const ctag_op_id = uuid();
	const ctag_inv_id = uuid();
	var res = await trans.send_as(founder, tx.company.TxCreatePrivate, {
		id,
		email: `${name}-${id}@${name}.org`,
		name,
		cost_tags: [ 
			{id: ctag_op_id, name: 'operating'},
			{id: ctag_inv_id, name: 'inventory'},
		],
		founder: {
			member_id: founder_uuid,
			occupation: workers[founder].occupation,
			default_cost_tags: [{id: ctag_op_id, weight: 10}],
		},
		created: new Date().toISOString(),
	});
	if(!res.success) {
		throw new Error('company.create() -- problem adding company: '+res.description);
	}

	await Promise.map(worker_ids, async function(wid) {
		if(wid == founder) return;
		const member_id = workers[wid].member_id;
		var res = await trans.send_as(founder, tx.company_member.TxCreate, {
			id: member_id,
			company_id: id,
			user_id: wid,
			roles: ['Owner'],
			occupation: workers[wid].occupation,
			default_cost_tags: [
				{id: ctag_op_id, weight: 10},
			],
			memo: 'adding team member '+workers[wid].name,
			created: new Date().toISOString(),
		});
		if(!res.success) {
			throw new Error('company.create() -- problem adding member: '+res.description);
		}
		console.log(`  + member: ${workers[wid].name} -- ${id}:${wid}`);
	});

	// mark it, dude
	const company = {
		id,
		name,
		transactions: [],
		products: {},
		cost_tags: {
			'operating': ctag_op_id,
			'inventory': ctag_inv_id,
		},
		workers: workers,
		labor: {},
		bus: new Emitter(),
	};
	company.bus.setMaxListeners(9999)
	companies[name] = company;
	exports.index(company);
};

exports.create_product = async function(company_name, product_name, options) {
	options || (options = {});
	const id = uuid();
	const company = companies[company_name];
	const worker_id = Object.keys(company.workers)[0];
	const cost_tags = (options.cost_tags || [{name: 'operating', weight: 1}, {name: 'inventory', weight: 1}])
		.map((entry) => {
			return {
				id: company.cost_tags[entry.name],
				weight: entry.weight,
			};
		});
	const inputs = (options.inputs || []).map((inp) => {
		return {
			product_id: companies[inp.company].products[inp.product].id,
			quantity: inp.amount,
		};
	});
	var res = await trans.send_as(worker_id, tx.product.TxCreate, {
		id,
		company_id: company.id,
		name: product_name,
		unit: options.unit,
		mass_mg: options.mass_mg,
		dimensions: {
			width: (options.dimensions || {}).width || 1,
			height: (options.dimensions || {}).height || 1,
			length: (options.dimensions || {}).length || 1,
		},
		cost_tags: cost_tags,
		active: true,
		meta: '{}',
		created: new Date().toISOString(),
	});
	if(!res.success) {
		throw new Error('company.create_product() -- problem adding product: '+res.description);
	}
	console.log(`+ product: ${product_name} (${company_name}) -- ${id}`);
	if(options.resource) {
		const tag_id = uuid();
		var res = await trans.send_as(worker_id, tx.resource_tag.TxCreate, {
			id: tag_id,
			product_id: id,
			created: new Date().toISOString(),
		});
		if(!res.success) {
			throw new Error('company.create_product() -- problem tagging product as resource: '+res.description);
		}
	}
	const product = {
		id,
		name: product_name,
		company_id: company.id,
		company_name,
		inputs: options.inputs || [],
	};
	companies[company_name].products[product_name] = product;
	products_idx_id[product.id] = product;
};

exports.setup_order = function(company_name, producer_name, product_name, options) {
	options || (options = {});
	companies[company_name].transactions.push({
		producer: producer_name,
		product: product_name,
		options,
	});
};

async function order_product(company_id_from, product_id, amount, category, options) {
	options || (options = {});
	const company_from = companies_idx_id[company_id_from];
	const product = products_idx_id[product_id];
	const company_to = companies_idx_id[product.company_id];

	console.log(`> order: ${company_from.name} -> ${company_to.name} -- ${product.name}: +${amount}`);
	const worker_id = Object.keys(company_from.workers)[0];
	const order_id = uuid();
	var res = await trans.send_as(worker_id, tx.order.TxCreate, {
		id: order_id,
		company_id_from: company_from.id,
		company_id_to: company_to.id,
		cost_tags: [
			{id: company_from.cost_tags[category], weight: 10},
		],
		products: [{
			product_id: product.id,
			quantity: amount,
		}],
		created: new Date().toISOString(),
	});
	if(res.success) {
		company_to.bus.emit('order:incoming', order_id);
		company_from.bus.once('order:'+order_id+':complete', options.complete || function() {});
		company_from.bus.once('order:'+order_id+':delivered', options.delivered || function() {});
	} else {
		logerr('order::create', res);
		throw new Error('order: '+res.description);
	}
	return res;
}
exports.order_product = order_product;

async function receive_order(order_id) {
	try {
		var order = await Basis.models.orders.get({id: order_id});
	} catch(err) {
		logerr('company::receive_order::get', order_id, err);
		throw err;
	}
	const company = companies_idx_id[order.company_id_to];
	const company_from = companies_idx_id[order.company_id_from];
	const worker = Object.keys(company.workers)[0];
	// we only have one product per order currenctly so this is easy
	const product = products_idx_id[order.products[0].product_id];
	const amount = order.products[0].quantity;
	// custom order-processing logic. obvis should be defined when the company
	// is created, but for now we work with this
	const update_order = async (status, options) => {
		options || (options = {});
		try {
			var res = await trans.send_as(worker, tx.order.TxUpdateStatus, {
				id: order_id,
				process_status: status,
				updated: last_time.toISOString(),
			});
			if(!res.success) {
				throw res;
			}
			if(options.wait) {
				await wait_ticks(company, options.wait);
			}
		} catch(err) {
			logerr('company::receive_order::update_status', order_id, status, err);
			throw err;
		}
	};

	const is_type = (...types) => types.indexOf(company.name) >= 0;
	const wait_opts = is_type('grocer') ? {} : {wait: 1};

	// adjust our inventory
	product.inputs.forEach((inp) => {
		const inp_product = companies[inp.company].products[inp.product];
		inc_inventory(company, inp_product.id, -amount);
	});

	await update_order('accepted', wait_opts);
	company.bus.emit('order:start');

	if(!is_type('grocer', 'power-planty')) {
		// wait 3 days. we kind of just assume infinite scale at each company
		// for now (obviously eventually we'll want to implement some form of
		// order queuing)
		await wait_ticks(company, 3 * 24);
	}

	await update_order('completed', wait_opts);
	company.bus.emit('order:complete');
	company_from.bus.emit('order:'+order_id+':complete');

	await update_order('proxying', wait_opts);
	company.bus.emit('order:ship');

	if(!is_type('grocer', 'power-planty')) {
		// shipping time, if applicable
		await wait_ticks(company, 1 * 24);
	}

	// always wait an hours to finalize an order.
	await wait_ticks(company, 1);

	await update_order('finalized', wait_opts);
	company_from.bus.emit('order:'+order_id+':delivered');
}

function setup_inventory(company) {
	// this is how much "extra" of something a company orders to keep inventory
	// stocks up.
	// TODO: manage this dynamically based on moving average of demand levels
	company.inventory_threshold = 200;

	const min = {};
	company.inventory = {};
	company.inventory_min = min;
	company.inventory_pending = {};
	Object.keys(company.products).forEach((name) => {
		const product = company.products[name];
		product.inputs.forEach((inp) => {
			const key = companies[inp.company].products[inp.product].id;
			const amount = inp.amount * company.inventory_threshold;
			if(!min[key]) min[key] = 0;
			min[key] += amount;
		});
	});

	Object.keys(min).forEach((product_id) => {
		if(min[product_id] < 100) min[product_id] = 100;
		set_inventory(company, product_id, min[product_id]);
	});
}

function set_inventory(company, product_id, amount) {
	if(!company.inventory) company.inventory = {};
	company.inventory[product_id] = amount;
}

function inc_inventory(company, product_id, amount) {
	if(!company.inventory) company.inventory = {};
	if(typeof(company.inventory[product_id]) == 'undefined') {
		throw new Error('inc_inventory() -- trying to increase inventory on product that is not in inventory');
	}
	company.inventory[product_id] += amount;
}

function manage_inventory(company) {
	const inventory = company.inventory || {};
	const min = company.inventory_min;
	const orders = [];
	Object.keys(inventory).forEach((product_id) => {
		if(company.inventory_pending[product_id]) return;
		const amount = inventory[product_id];
		const diff = amount - min[product_id];
		if(diff < 0) {
			const order_amount = -diff + (min[product_id] / company.inventory_threshold);
			console.log(`> inventory: ${company.id} -- ${product_id}: ${order_amount} (diff ${diff})`);
			company.inventory_pending[product_id] = true;
			const promise = order_product(company.id, product_id, order_amount, 'inventory', {
				delivered: function() {
					inc_inventory(company, product_id, order_amount);
					delete company.inventory_pending[product_id];
				},
			});
			orders.push(promise);
		}
	});
	return Promise.all(orders);
}

async function manage_recurring_orders(now, company) {
	try {
		const promises = company.transactions
			.filter((trans) => {
				return cron.matches(now, trans.options.period);
			})
			.map(async (trans) => {
				const {producer: company_to_name, product: product_name, options} = trans;
				const company_to = companies[company_to_name];
				const product = company_to.products[product_name];
				return order_product(company.id, product.id, options.amount, 'operating');
			});
		await Promise.all(promises);
	} catch(err) {
		logerr('company::recurring_orders', company.name, err);
	}
}

async function manage_labor(now, company) {
	const promises = Object.keys(company.workers).map(async (worker_id) => {
		if(cron.matches(now, '0 0 9 * * *')) {
			if(company.labor[worker_id]) {
				//console.log(`* clock in: ${company.name} -- ${company.workers[worker_id].name} is already clocked in...????`);
				return;
			}
			// was nice sleep and gud toast but now is time for serious wurk
			const labor_id = uuid();
			var res = await trans.send_as(worker_id, tx.labor.TxCreate, {
				id: labor_id,
				company_id: company.id,
				user_id: worker_id,
				created: now.toISOString(),
			});
			if(res.success) {
				//console.log(`> clock in: ${company.name} -- ${company.workers[worker_id].name}`);
				company.labor[worker_id] = labor_id;
			} else {
				throw res;
			}
		}
		if(cron.matches(now, '0 0 17 * * *')) {
			// wow 8 hrs wut a day tiem to go home
			const labor_id = company.labor[worker_id];
			if(!labor_id) {
				//console.log(`* clock out: ${company.name} -- ${company.workers[worker_id].name} is not clocked in...????`);
				return;
			}
			var res = await trans.send_as(worker_id, tx.labor.TxUpdate, {
				id: labor_id,
				start: new Date(0).toISOString(),
				end: now.toISOString(),
				updated: now.toISOString(),
			});
			if(res.success) {
				//console.log(`> clock out: ${company.name} -- ${company.workers[worker_id].name}`);
				delete company.labor[worker_id];
			} else {
				throw res;
			}
		}
	});
	try {
		await Promise.all(promises);
	} catch(err) {
		logerr('company::labor', company.name, err);
	}
}

async function company_tick(now, company) {
	company.bus.emit('tick');
	// only run recurring orders after the first day. the purpose is to let
	// companies get some labor costs under their belts to avoid the dreaded
	// "Product has no costs" error
	let recurring_promise = null;
	if(now - start >= (86400 * 1000)) {
		recurring_promise = manage_recurring_orders(now, company);
	}
	const labor_promise = manage_labor(now, company);
	await Promise.all([recurring_promise, labor_promise]);
}

async function wait_ticks(company, ticks) {
	var count = 0;
	return new Promise((resolve) => {
		const cb = () => {
			count++;
			if(count >= ticks) {
				resolve();
				company.bus.off('tick', cb);
			}
		};
		company.bus.on('tick', cb);
	});
}

function company_runner(bus, name) {
	console.log('> run: company: ', name);
	const company = companies[name];
	setup_inventory(company);
	bus.on('tick', function(now) {
		last_time = now;
		return company_tick(now, company)
			.catch((err) => {
				logerr('company::runner::tick', company.name, err);
			});
	});
	company.bus.on('order:incoming', function(order_id) {
		return receive_order(order_id)
			.catch((err) => {
				logerr('company::runner::recv_order', order_id, company.name, err);
			});
	});
	company.bus.on('order:start', function() {
		manage_inventory(company)
			.catch((err) => {
				logerr('company::runner::inventory', company.name, err);
			});
	});
	return new Promise(function(resolve) {
		bus.on('quit', resolve);
	});
}

exports.run = async function(bus, start_) {
	start = start_;
	return Promise.map(Object.keys(companies), function(name) {
		return company_runner(bus, name);
	});
};


const moment = require('moment');
const cron_parser = require('cron-parser');

exports.matches = function(time, cronspec) {
	const crontime = moment(time).subtract(1, 'seconds');
	const cron = cronspec && cron_parser.parseExpression(cronspec, {
		currentDate: new Date(crontime.toISOString()),
	});
	return cron.next().toISOString() == time.toISOString();
};

